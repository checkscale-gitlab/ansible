# Ansible-playbook docker image

This is a docker container to run Ansible playbooks as there isn't an official one.

* `entrypoint` script creates `ansible` user (with `ansible` group) on the fly.
* `AS_UID` envar can be used to customize `ansible` user UID, defautl is `1000`.
* `ansible` group members can sudo without password.
* `entrypoint` run `ansible-playbook` as `ansible` user, it can receive command parameters. 
* `/project` set as working directory, mount your ansible project there as volume.

## Usage examples

Project that follows [Ansible's directory layaut best practices](https://docs.ansible.com/ansible/2.3/playbooks_best_practices.html#directory-layout) matching UID with your host user:
```
cd /path/to/ansible/project
```
```
ls
ansible.cfg  playbook.yml
```
```
docker run -ti --rm -e AS_UID=$(id -u) -v $PWD:/project registry.gitlab.com/live9/docker/images/ansible playbook.yml
```

Project with playbooks in a separate directory
```
cd /path/to/ansible/project
```
```
ls -F
playbooks/  inventory  variables.yml
```
```
ls playbooks/
anotherplaybook.yml  playbook.yml
```
```
docker run -ti --rm -v $PWD:/project --workdir /project/playbooks registry.gitlab.com/live9/docker/images/ansible --inventory ../inventory --extra-vars @../variables.yml playbook.yml
```
